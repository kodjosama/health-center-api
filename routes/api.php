<?php

use App\Http\Controllers\BulkHealthCenterController;
use App\Http\Controllers\HealthCenterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::apiResource('health-centers', HealthCenterController::class)->only('index', 'store', 'update');

Route::post('health-centers/bulk', BulkHealthCenterController::class);
