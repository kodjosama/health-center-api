<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBulkHealthCenterRequest;
use App\Models\HealthCenter;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BulkHealthCenterController extends Controller
{
    public function __invoke(StoreBulkHealthCenterRequest $request)
    {
        DB::transaction(function () use ($request) {
            collect($request->validated('centers'))
                ->each(fn ($center) => HealthCenter::create($center));
        });

        return new JsonResponse(status:204);
    }
}
