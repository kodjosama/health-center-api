<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreHealthCenterRequest;
use App\Http\Requests\UpdateHealthCenterRequest;
use App\Http\Resources\HealthCenterResource;
use App\Models\HealthCenter;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class HealthCenterController extends Controller
{
    public function index()
    {
        return HealthCenterResource::collection(HealthCenter::get());
    }

    public function store(StoreHealthCenterRequest $request)
    {
        $center = DB::transaction(fn () => HealthCenter::create($request->validated()));

        return new HealthCenterResource($center);
    }

    public function update(StoreHealthCenterRequest $request, HealthCenter $healthCenter)
    {
        DB::transaction(fn () => $healthCenter->fill($request->validated())->save());

        return new HealthCenterResource($healthCenter->refresh());
    }
}
