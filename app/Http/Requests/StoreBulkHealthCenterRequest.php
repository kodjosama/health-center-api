<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBulkHealthCenterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'centers' => [
                'required',
                'array',
                'min:1',
            ],
            'centers.*.name' => [
                'required',
                'string',
                'min:3',
                'max:200',
            ],
            'centers.*.phone_number' => [
                'required',
                'string',
                'min:3',
                'max:200',
            ],
            'centers.*.city' => [
                'required',
                'string',
                'min:3',
                'max:200',
            ],
            'centers.*.address' => [
                'required',
                'string',
                'min:3',
            ],
        ];
    }
}
