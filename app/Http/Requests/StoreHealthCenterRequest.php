<?php

namespace App\Http\Requests;

use App\Models\HealthCenter;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreHealthCenterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'min:3',
                'max:200',
            ],
            'phone_number' => [
                'required',
                'string',
                'min:3',
                'max:200',
            ],
            'city' => [
                'required',
                'string',
                'min:3',
                'max:200',
            ],
            'address' => [
                'required',
                'string',
                'min:3',
            ],
        ];
    }
}
